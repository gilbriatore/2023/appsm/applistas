package br.briatore.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Toast
import br.briatore.app.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val estados = mapOf(
            "AC" to "Acre",
            "AL" to "Alagoas",
            "AM" to "Amazonas",
            "AP" to "Amapá",
            "BA" to "Bahia",
            "CE" to "Ceará",
            "DF" to "Distrito Federal",
            "ES" to "Espírito Santo",
            "GO" to "Goiás",
            "MA" to "Maranhão",
            "MG" to "Minas Gerais",
            "MS" to "Mato Grosso do Sul",
            "MT" to "Mato Grosso",
            "PA" to "Pará",
            "PB" to "Paraíba",
            "PE" to "Pernambuco",
            "PI" to "Piauí",
            "PR" to "Paraná",
            "RJ" to "Rio de Janeiro",
            "RN" to "Rio Grande do Norte",
            "RO" to "Rondônia",
            "RR" to "Roraima",
            "RS" to "Rio Grande do Sul",
            "SC" to "Santa Catarina",
            "SE" to "Sergipe",
            "SP" to "São Paulo",
            "TO" to "Tocantins"
        )

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val adapter = ArrayAdapter.createFromResource(
            applicationContext, R.array.siglas,
            com.google.android.material.R.layout.support_simple_spinner_dropdown_item
        )
        binding.spinner.adapter = adapter

        val vetor = resources.getStringArray(R.array.siglas)
        binding.spinner.onItemSelectedListener = object : OnItemSelectedListener{

            override fun onItemSelected(parent: AdapterView<*>?, view: View?,
                position: Int, id: Long
            ) {
                val sigla = vetor[position]
                val estado = estados.get(sigla)
                Toast.makeText(applicationContext, estado, Toast.LENGTH_LONG).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

    }
}